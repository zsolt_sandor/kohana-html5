<?php defined('SYSPATH') or die('No direct script access.');

class HTML5_Form extends Kohana_Form {

  /**
   * Creates a checkbox form input.
   *
   *     echo Form::checkbox('remember_me', 1, (bool) $remember);
   *
   * @param   string   input name
   * @param   string   input value
   * @param   boolean  checked status
   * @param   array    html attributes
   * @return  string
   * @uses    Form::input
   */
  public static function checkbox($name, $value = NULL, $checked = FALSE, array $attributes = NULL)
  {
    $attributes['type'] = 'checkbox';

    if ($checked === TRUE)
    {
      // Make the checkbox active
      $attributes['checked'] = TRUE;
    }

    return Form::input($name, $value, $attributes);
  }

  /**
    * Creates an email form input.
    *
    * echo Form::email('email');
    *
    * @param string input name
    * @param string input value
    * @param array html attributes
    * @return string
    * @uses Form::input
    */
  public static function email($name, $value = NULL, array $attributes = NULL)
  {
    $attributes['type'] = 'email';
    return Form::input($name, $value, $attributes);
  }

}